# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

reset_db() {
  bundle exec rake db:drop db:create db:migrate
  RAILS_ENV=test bundle exec rake db:drop db:create db:migrate
}

reseed_db() {
  bundle exec rake db:drop db:create db:migrate
  bundle exec rake db:seed
  RAILS_ENV=test bundle exec rake db:drop db:create db:migrate
  RAILS_ENV=test bundle exec rake db:seed
}

grelease() {
  # grunt and release to community cloud
  cd ~/json-editor
  grunt
  cp ~/json-editor/dist/jsoneditor.js ~/grasshopper_dev/community_cloud/vendor/assets/javascripts/jsoneditor.js
  git checkout dist/

}

shorttest() {
  SHORT_TESTS=1 bundle exec rspec spec
}

longtest() {
  LONG_TESTS=1 bundle exec rspec spec
}

alltest() {
  bundle exec rspec spec
}

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="jonathan"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Uncomment this to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment following line if you want to  shown in the command execution time stamp 
# in the history command output. The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|
# yyyy-mm-dd
HIST_STAMPS="mm/dd/yyyy"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git tmux bundler rails zeus history history-substring-search)

source $ZSH/oh-my-zsh.sh
source $ZSH/aliases.sh

# # Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"


[[ -s "$HOME/.profile" ]] && source "$HOME/.profile" # Load the default .profile

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

/usr/bin/ssh-add
/usr/bin/ssh-add ~/.ssh/AWSGRASS.pem
cd ~/grasshopper_dev/community_cloud


# User Variables
cc=/home/jason/grasshopper_dev/community_cloud
us=/home/jason/grasshopper_dev/gh_user_service
dp=/home/jason/dog_parks

# Use vi binding

bindkey -v

bindkey '^P' history-substring-search-up
bindkey '^N' history-substring-search-down
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey '^w' backward-kill-word
bindkey '^r' history-incremental-search-backward

function zle-line-init zle-keymap-select {
  VIM_PROMPT="%{$fg_bold[yellow]%} [% NORMAL]% %{$reset_color%}"
  # RPS1="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/} $(git_custom_status) $EPS1"
  RPS1="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/} $EPS1"
  zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select
export KEYTIMEOUT=1
